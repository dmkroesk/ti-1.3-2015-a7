package AgendaModule;
// import java.awt.Image;

//image heb ik er heel ff uitgeknikkert. :D
public class Artist {

    private String name;
    private String information = "";

    public Artist(String name)
    {
	this.name = name;
	this.information = "nothing added yet";
    }

    public String getName() {
	return name;
    }

    public void addInformation(String info) {
	this.information = info;
    }

    public String getInformation() {
	return information;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String toString() {
	return "//" + name + "//" + information;
    }
}
