package AgendaModule;

public class Stage {

    private String stageName;
    private int stageNumber;

    public Stage(String stageName, int stageNumber) {
	this.stageName = stageName;
	this.stageNumber = stageNumber;

    }

    public String getName() {
	return stageName;
    }

    public int getNumber() {
	return stageNumber;
    }

    public String toString() {
	return stageName + "//" + stageNumber;
    }

    public void setName(String name) {
	this.stageName = name;
    }
    
    public String getClassName()
    {
    	return "stage";
    }
}
