package AgendaModule;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.*;

public class GUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private Buttons button = new Buttons(this);
    private int posX = 0, posY = 0;
    protected ArrayList<Act> acts;
    private ArrayList<Artist> artists = new ArrayList<>();
    private ArrayList<Stage> stages = new ArrayList<>();
    private SimpleDateFormat date = new SimpleDateFormat("HH:mm");
    private JTable table;
    private JPanel tabPanel;
    private JPanel bottomPanel;

//    private int number = 0;

    public GUI() {
	super("Calendar");
	this.acts = new ArrayList<Act>();

	try {
	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e) {
	}
	frame();
	drag();
	pack();
	setSize(640, 480);
    }

    public void frame() {
	setDefaultCloseOperation(EXIT_ON_CLOSE);

	JLabel background = new JLabel(new ImageIcon(
		"src/images/BackgroundGui.jpg"));
	background.setLayout(new BorderLayout());

	this.tabPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	tabPanel.setBackground(Color.white);

	this.bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
	bottomPanel.setBackground(Color.white);

	this.table = new JTable(new MyTableModel(this));

	table.setAutoCreateRowSorter(true);
	table.setEnabled(false);
	table.setOpaque(false);

	JScrollPane scrollPane = new JScrollPane(table);
	scrollPane.setOpaque(false);
	scrollPane.getViewport().setOpaque(false);

	background.add(bottomPanel, BorderLayout.SOUTH);
	background.add(tabPanel, BorderLayout.NORTH);
	background.add(scrollPane, BorderLayout.CENTER);

	table.repaint();

	addButtons();
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int width = (int) (screenSize.getWidth() / 2) - 340;
	int height = (int) (screenSize.getHeight() / 2) - 220;
	setUndecorated(true);
	setLocation(width, height);
	setContentPane(background);
	setVisible(true);
    }

    private void addButtons() {
	tabPanel.add(button.newFile());
	tabPanel.add(button.save());
	tabPanel.add(button.load());
	tabPanel.add(button.Simulator());
	tabPanel.add(button.info());
	tabPanel.add(button.quit());

	bottomPanel.add(button.addStageOrArtistButton("Add Stage"));
	bottomPanel.add(button.addStageOrArtistButton("Add Artist"));
	bottomPanel.add(button.addAct());
	bottomPanel.add(button.edit());
	bottomPanel.add(button.remove());
    }

    public void drag() {
	this.addMouseListener(new MouseAdapter() {
	    public void mousePressed(MouseEvent e) {
		posX = e.getX();
		posY = e.getY();
	    }
	});
	this.addMouseMotionListener(new MouseAdapter() {
	    public void mouseDragged(MouseEvent evt) {
		setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen()
			- posY);
	    }
	});
    }

    protected void addStageOrArtist(String item) {
	String option = JOptionPane.showInputDialog("What is the name of the " + item.toLowerCase() + "?");
	if (option == null) {
	    return;
	}

	if (item.toLowerCase().equals("artist")) {
	    for (Act a : acts) {
		if (option.equals(a.getArtistName())) {
		    JOptionPane.showMessageDialog(null,
			    "Name already exist!");
		    return;
		}
	    }

	    for (Artist a : artists) {
		if (option.equals(a.getName())) {
		    JOptionPane.showMessageDialog(null,
			    "Name already excist!");
		    return;
		}
	    }
	    artists.add(new Artist(option));
	} else if (item.toLowerCase().equals("stage")) {
	    for (Act a : acts) {
		if (option.equals(a.getStageName())) {
		    JOptionPane.showMessageDialog(null,
			    "Stage already exist!");
		    return;
		}
	    }
	    for (Stage a : stages) {
		if (option.equals(a.getName())) {
		    JOptionPane.showMessageDialog(null,
			    "Stage already exist!");
		    return;
		}
	    }
	    stages.add(new Stage(option, acts.size()));
	} else {
	    JOptionPane.showMessageDialog(null, "Bad programming, blame the monkeys");
	}
    }

    protected void edit() {
	JPanel radioArray = new JPanel(new GridLayout(0, 1));
	ButtonGroup group = new ButtonGroup();
	AbstractButton but1 = new JRadioButton("Edit Act");
	group.add(but1);
	radioArray.add(but1);
	AbstractButton but2 = new JRadioButton("Edit Stagename");
	group.add(but2);
	radioArray.add(but2);
	AbstractButton but3 = new JRadioButton("Edit Artistname");
	group.add(but3);
	radioArray.add(but3);
	group.setSelected(but1.getModel(), true);

	int option3 = JOptionPane.showConfirmDialog(this, radioArray,
		"Edit Act", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	if (option3 == JOptionPane.CANCEL_OPTION) {
	    return;
	}

	for (int r = 0; r < group.getButtonCount(); r++) {
	    if (but1.isSelected()) {

		changeAct("edit");

	    } else if (but2.isSelected()) {
		HashSet<String> hs2 = new HashSet();
		if (acts.isEmpty() == false) {
		    for (Act act : acts) {
			hs2.add(act.getStageName());
		    }
		}
		if (stages.isEmpty() == false) {
		    for (Stage s : stages) {
			hs2.add(s.getName());
		    }
		}
		String[] stageArray = new String[hs2.size()];
		stageArray = hs2.toArray(stageArray);

		JComboBox stage = new JComboBox(stageArray);
		JTextField stageInput = new JTextField();
		stage.setEditable(false);

		Object[] message = {"Stage: ", stage,
		    "New stagename: ", stageInput};
		int option = JOptionPane.showConfirmDialog(null,
			message, "Edit Stage",
			JOptionPane.OK_CANCEL_OPTION);

		if (option == JOptionPane.CANCEL_OPTION) {
		    return;
		} else if (option == JOptionPane.OK_OPTION) {
		    if (stageInput.getText().equals(
			    stage.getSelectedItem().toString())) {
			JOptionPane.showMessageDialog(null, stageInput.getText()
				+ " is the same name as before.");
			return;
		    } else if (stageInput.getText().equals("")) {
			JOptionPane.showMessageDialog(null,
				"Please enter a name");
			return;
		    } else {
			for (int i = 0; i < stageArray.length; i++) {
			    if (stageArray[i].equals(stageInput
				    .getText())) {

				int option1 = JOptionPane
					.showConfirmDialog(
						null,
						stageInput.getText()
						+ " This stage name already exists. \n Do you want to overwrite it?");

				if (option1 == JOptionPane.CANCEL_OPTION) {
				    return;
				} else if (option1 == JOptionPane.NO_OPTION) {
				    return;
				}

			    }
			}
			for (Act a : acts) {
			    if (a.getStageName().equals(
				    stage.getSelectedItem().toString())) {
				a.setStageName(stageInput.getText());
			    }
			}
			for (Stage s : stages) {
			    if (s.getName().equals(
				    stage.getSelectedItem().toString())) {
				s.setName(stageInput.getText());
			    }
			}
			reloadGui();
			return;
		    }

		}

	    } else {
		HashSet<String> hs1 = new HashSet();
		if (acts.isEmpty() == false) {
		    for (Act act : acts) {
			hs1.add(act.getArtistName());
		    }
		}
		if (artists.isEmpty() == false) {
		    for (Artist a1 : artists) {
			hs1.add(a1.getName());
		    }
		}
		String[] artistArray = new String[hs1.size()];
		artistArray = hs1.toArray(artistArray);

		JComboBox artist = new JComboBox(artistArray);
		JTextField artistInput = new JTextField();
		artist.setEditable(false);

		Object[] message = {"Artist: ", artist,
		    "New artistname: ", artistInput};

		int option = JOptionPane.showConfirmDialog(null,
			message, "Edit Artist",
			JOptionPane.OK_CANCEL_OPTION);

		if (option == JOptionPane.CANCEL_OPTION) {
		    return;
		} else if (option == JOptionPane.OK_OPTION) {
		    if (artistInput.getText().equals(
			    artist.getSelectedItem().toString())) {
			JOptionPane.showMessageDialog(null, artistInput.getText()
				+ " is the same name as before.");
			return;
		    } else if (artistInput.getText().equals("")) {
			JOptionPane.showMessageDialog(null,
				"Please enter a name");
			return;
		    } else {
			for (int i = 0; i < artistArray.length; i++) {
			    if (artistArray[i].equals(artistInput.getText())) {

				int option1 = JOptionPane
					.showConfirmDialog(
						null,
						artistInput.getText()
						+ " This artist name already exists. \n Do you want to overwrite it?");

				if (option1 == JOptionPane.CANCEL_OPTION) {
				    return;
				} else if (option1 == JOptionPane.NO_OPTION) {
				    return;
				}

			    }
			}
			for (Act a : acts) {
			    if (a.getArtistName()
				    .equals(artist.getSelectedItem()
					    .toString())) {
				a.setArtistName(artistInput.getText());
			    }
			}
			for (Artist a1 : artists) {
			    if (a1.getName()
				    .equals(artist.getSelectedItem()
					    .toString())) {
				a1.setName(artistInput.getText());
			    }
			}
			reloadGui();
			return;
		    }
		}
	    }
	}
    }

    protected void removeAct() {
	if (acts.size() <= 0) {
	    JOptionPane.showMessageDialog(null, "There are no acts to edit", "Edit act", JOptionPane.CANCEL_OPTION);
	    return;
	} else {
	    JComboBox numbers = new JComboBox();

	    for (int i = 1; i <= acts.size(); i++) {
		numbers.addItem(i);
	    }

	    int option = JOptionPane.showConfirmDialog(null, numbers, "Which actnumber?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
	    if (option == JOptionPane.CANCEL_OPTION) {
		return;
	    }
	    acts.remove(option);
	    reloadGui();

	}
    }

    protected void saveCalendar(String file) {
	if (!file.endsWith(".a7")) {
	    file = file + ".a7";
	}

	try {
	    FileWriter writer = new FileWriter(file);
	    for (int i = 0; i < acts.size(); i++) {
		writer.write(acts.get(i).toString());
		writer.write('\n');
	    }
	    writer.close();
	} catch (IOException e) {

	}
    }

    private void reloadGui() {
	((MyTableModel) table.getModel()).fireTableDataChanged();
	table.repaint();
    }

    protected void clearGui() {
	acts.clear();
	stages.clear();
	artists.clear();
	reloadGui();
    }

    protected void loadCalendar(File file) {
	try {
	    clearGui();
	    @SuppressWarnings("resource")
	    Scanner inputfile = new Scanner(file);
	    while (inputfile.hasNextLine()) {

		String line = inputfile.nextLine();
		String[] words = line.split("//");
		int i = 0;
		if (words.length >= 5) {
		    String startTime = words[i];
		    i++;
		    String endTime = words[i];
		    i++;
		    String nameArtist = words[i];
		    i++;
		    String infoArtist = words[i];
		    i++;
		    String stageName = words[i];
		    i++;
		    int stageNumber = Integer.parseInt(words[i]);
		    i++;
		    int fame = Integer.parseInt(words[i]);

		    Artist artist = new Artist(nameArtist);
		    if (!infoArtist.equals("nothing added yet"));

		    artist.addInformation(infoArtist);
		    acts.add(new Act(new Stage(stageName, stageNumber),
			    new Time(startTime, endTime), artist, fame));
		}

		reloadGui();
	    }

	} catch (FileNotFoundException e) {
	}
    }

    private String[] fillComboBox(String type) {
	HashSet<String> hs = new HashSet();
	if (type.toUpperCase().equals("ARTIEST")) {
	    if (acts.isEmpty() == false) {
		for (Act act : acts) {
		    hs.add(act.getArtistName());
		}
	    }
	    if (artists.isEmpty() == false) {
		for (Artist artist : artists) {
		    hs.add(artist.getName());
		}
	    }
	    String[] artiesten = new String[hs.size()];
	    artiesten = hs.toArray(artiesten);
	    return artiesten;
	} else if (type.toUpperCase().equals("STAGE")) {
	    if (acts.isEmpty() == false) {
		for (Act act : acts) {
		    hs.add(act.getStageName());
		}
	    }
	    if (stages.isEmpty() == false) {
		for (Stage stage : stages) {
		    hs.add(stage.getName());
		}
	    }
	    String[] stages = new String[hs.size()];
	    stages = hs.toArray(stages);
	    return stages;
	} else {
	    String[] a = {"Bad programming.", "Blame the sheep!"};
	    return a;
	}
    }

    protected void changeAct(String editOrNew) {
	if (editOrNew.toLowerCase().equals("edit")) {
	    if (acts.size() <= 0) {
		JOptionPane.showMessageDialog(null, "There are no acts to edit", "Edit act", JOptionPane.CANCEL_OPTION);
		return;
	    } else {
		JComboBox numbers = new JComboBox();

		for (int i = 1; i <= acts.size(); i++) {
		    numbers.addItem(i);
		}

		int option = JOptionPane.showConfirmDialog(null, numbers, "Which actnumber?", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (option == JOptionPane.CANCEL_OPTION) {
		    return;
		} else {
		    imOutOfMethodnameIdeasButItHasToDoWithAddingOrChangingActs(option, "Edit act");
		}
	    }
	} else if (editOrNew.toLowerCase().equals("new")) {
	    imOutOfMethodnameIdeasButItHasToDoWithAddingOrChangingActs(acts.size(), "New act");
	} else {
	    throw new IllegalArgumentException("Bad Programming, blame the sheep!");
	}
    }

    public ArrayList passArray() {
	return acts;
    }

    private void imOutOfMethodnameIdeasButItHasToDoWithAddingOrChangingActs(int actsIndex, String dialogName) {
	Act a = null;
	Date startTime = null;
	Date endTime = null;
	try {
	    a = acts.get(actsIndex);
	} catch (IndexOutOfBoundsException e) {
	    Date newTime;
	    try {
		newTime = date.parse("0:00");
	    } catch (ParseException ex) {
		return;
	    }
	    a = new Act(new Stage("", 0), new Time(newTime, newTime), new Artist(""), 50);
	} finally {
	    JComboBox artist = new JComboBox(fillComboBox("ARTIEST"));
	    JComboBox stage = new JComboBox(fillComboBox("STAGE"));
	    JTextField start = new JTextField();
	    JTextField end = new JTextField();
	    start.addKeyListener(new KeyAdapter() {
		public void keyTyped(KeyEvent e) {
		    char c = e.getKeyChar();
		    if (!((c >= '0' && c <= '9') || c == ':' || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER)) {
			JOptionPane.showMessageDialog(null, "Sorry, but that was not a valid key or format. \nValid keys: 0-9 and :, valid format: hours:minutes");
			e.consume();
		    }
		}
	    });
	    end.addKeyListener(new KeyAdapter() {
		public void keyTyped(KeyEvent e) {
		    char c = e.getKeyChar();
		    if (!((c >= '0' && c <= '9') || c == ':' || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER)) {
			JOptionPane.showMessageDialog(null, "Sorry, but that was not a valid key or format. \nValid keys: 0-9 and :, valid format: hours:minutes");
			e.consume();
		    }
		}
	    });

	    JComboBox fame = new JComboBox();
	    for (int i = 0; i <= 100; i++) {
		fame.addItem(i);
	    }
	    fame.setSelectedIndex(50);
	    fame.setEditable(false);
	    artist.setSelectedItem(a.getArtistName());
	    stage.setSelectedItem(a.getStageName());
	    artist.setEditable(true);
	    stage.setEditable(true);

	    if (!a.getArtistName().equals("")) {
		start.setText(a.getStart());
		end.setText(a.getEnd());
		fame.setSelectedIndex(a.getFame());
	    }

	    Object[] message = {"Stage: ", stage,
		"Artist: ", artist, "Start time: ",
		start, "End time: ", end, "Fame: ", fame};

	    int option = JOptionPane.showConfirmDialog(null, message,
		    dialogName, JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

	    if (option == JOptionPane.CANCEL_OPTION || option == JOptionPane.CLOSED_OPTION) {
		return;
	    }

	    if (start.getText().equals("") || end.getText().equals("") || artist.getSelectedItem() == null || stage.getSelectedItem() == null) {
		JOptionPane.showMessageDialog(null, "You have not entered everything");
		return;
	    }

	    if (!(checkTime(start.getText()) && checkTime(end.getText()))) {
		JOptionPane.showMessageDialog(null, "You have entered an invalid time");
		return;
	    }

	    try {
		startTime = date.parse(start.getText());
		endTime = date.parse(end.getText());
	    } catch (ParseException e) {
		JOptionPane.showMessageDialog(null, "You have entered an invalid time");
		return;
	    }

	    if (startTime.compareTo(endTime) >= 0) {
		JOptionPane.showMessageDialog(null, "Your endtime is before your starttime");
		return;
	    }

	    a.setArtistName(artist.getSelectedItem().toString());
	    a.setStageName(stage.getSelectedItem().toString());
	    a.setStart(startTime);
	    a.setEnd(endTime);
	    a.setFame(fame.getSelectedIndex());

	    if (actsIndex < acts.size()) {
		Act b = acts.get(actsIndex);
		b = a;
	    } else {
		acts.add(a);
	    }

	    reloadGui();
	}
    }

    private boolean checkTime(String time) {
	boolean bool = true;
	if (time.contains(":")) {
	    String[] parts = time.split(":");
	    int part1 = Integer.parseInt(parts[0]);
	    int part2 = Integer.parseInt(parts[1]);
	    if ((part1 <= -1 || part1 >= 24) || (part2 <= -1 || part2 >= 60)) {
		bool = false;
	    }
	} else {
	    bool = false;
	}
	return bool;
    }
    
    public ArrayList<Act> getActs()
    {
    	return acts;
    }
}
