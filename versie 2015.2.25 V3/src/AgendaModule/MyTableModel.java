package AgendaModule;

import javax.swing.table.AbstractTableModel;

class MyTableModel extends AbstractTableModel {

    private String[] columnNames = {"Actnumber", "Stage", "Artist",
	"Start time", "End time", "Fame"};

    private GUI gui;

    public MyTableModel(GUI gui) {
	this.gui = gui;
    }

    public String getColumnName(int col) {
	return columnNames[col];
    }

    @Override
    public int getColumnCount() {
	return columnNames.length;
    }

    @Override
    public int getRowCount() {
	return gui.acts.size();
    }

    @Override
    public boolean isCellEditable(int row, int col) {
	return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
	Act a = gui.acts.get(rowIndex);
	switch (columnIndex) {
	    case 0:
		return rowIndex + 1;
	    case 1:
		return a.getStageName();
	    case 2:
		return a.getArtistName();
	    case 3:
		return a.getStart();
	    case 4:
		return a.getEnd();
	    case 5:
		return a.getFame();
	    default:
		return "Invalid";
	}
    }
    
    

}
