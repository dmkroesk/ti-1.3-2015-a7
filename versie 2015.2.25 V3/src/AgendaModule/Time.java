package AgendaModule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {

    SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
    private String startTime;
    private String endTime;

    public Time(Date startTimer, Date endTimer) {
	/*try {
	 this.startTime = parser.parse(startTime);
	 } catch (ParseException e) {
	 // TODO Auto-generated catch block
	 e.printStackTrace();
	 }*/
	String startToString = parser.format(startTimer);
	String endToString = parser.format(endTimer);
	this.startTime = startToString;
	this.endTime = endToString;
    }

    public Time(String startTimer, String endTimer) {
	/*try {
	 this.startTime = parser.parse(startTime);
	 } catch (ParseException e) {
	 // TODO Auto-generated catch block
	 e.printStackTrace();
	 }*/
	String startToString = startTimer;
	String endToString = endTimer;
	this.startTime = startToString;
	this.endTime = endToString;
    }

    @SuppressWarnings("deprecation")
    public String getLength() {
	Date endTimed = new Date(endTime);
	try {
	    endTimed = parser.parse(endTime);
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	Date startTimed = new Date(startTime);

	try {
	    startTimed = parser.parse(startTime);
	} catch (ParseException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	String minutes;
	int seconds = (int) ((endTimed.getTime() - startTimed.getTime())) / 1000;
	int minutesInt = seconds / 60;
	int hours = minutesInt / 60;
	minutesInt = minutesInt % 60;

	if (minutesInt < 10) {
	    minutes = "0" + minutesInt;
	} else {
	    minutes = "" + minutesInt;
	}

	return hours + ":" + minutes;
    }

    public void setStart(Date startTimer) {
	String startToString = parser.format(startTimer);
	this.startTime = startToString;
    }

    public void setEnd(Date endTimer) {
	String endToString = parser.format(endTimer);
	this.endTime = endToString;
    }

    public String getStart() {
	return startTime;
    }

    public String getEnd() {
	return endTime;
    }

    public String toString() {
	return startTime + "//" + endTime;
    }
}
