package AgendaModule;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import Simulator.GuiSimulator;
import bezoeker.Bezoeker;

public class Buttons {

    protected GUI gui;
    public Bezoeker b;

    public Buttons(GUI gui) {
	this.gui = gui;
    }

    protected JButton info() {
	JButton info = new JButton("Info");
	info.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		JOptionPane.showMessageDialog(gui, "Made by A7");
	    }
	});

	info.setFont(new Font("Arial", Font.PLAIN, 20));
	info.setOpaque(false);
	info.setContentAreaFilled(false);
	info.setBorderPainted(false);
	return info;
    }

    protected JButton quit() {
	JButton quit = new JButton("Quit");
	quit.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		System.exit(0);
	    }
	});
	quit.setFont(new Font("Arial", Font.PLAIN, 20));
	quit.setOpaque(false);
	quit.setContentAreaFilled(false);
	quit.setBorderPainted(false);
	return quit;
    }

    protected JButton Simulator() {
	JButton simulator = new JButton("Simulator");
	simulator.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		new GuiSimulator(gui);
	    }
	});

	simulator.setFont(new Font("Arial", Font.PLAIN, 20));
	simulator.setOpaque(false);
	simulator.setContentAreaFilled(false);
	simulator.setBorderPainted(false);
	return simulator;
    }

    protected JButton addAct() {
	JButton add = new JButton("Add Act");
	add.addActionListener(new ActionListener() {
	    @SuppressWarnings({"rawtypes", "unchecked"})
	    public void actionPerformed(ActionEvent event) {
		gui.changeAct("new");
	    }
	});

	add.setFont(new Font("Arial", Font.PLAIN, 20));
	add.setOpaque(false);
	add.setContentAreaFilled(false);
	add.setBorderPainted(false);
	return add;
    }

    protected JButton load() {
	JButton load = new JButton("Load");
	load.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		JFileChooser load = new JFileChooser(System
			.getProperty("user.dir"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(".a7", "a7");
		load.setFileFilter(filter);

		if (load.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
		    File file = load.getSelectedFile();
		    if (file.getName().endsWith(".a7")) {
			gui.loadCalendar(file);
		    } else {
			JOptionPane.showMessageDialog(gui, "Sorry, but this file is invalid");
		    }
		} else if (JFileChooser.CANCEL_OPTION == 0) {
		    return;
		}
	    }
	});

	load.setFont(new Font("Arial", Font.PLAIN, 20));
	load.setOpaque(false);
	load.setContentAreaFilled(false);
	load.setBorderPainted(false);
	return load;
    }

    protected JButton save() {
	JButton save = new JButton("Save");
	save.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {

		JFileChooser save = new JFileChooser(System
			.getProperty("user.dir"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
			".a7", "a7");
		save.setFileFilter(filter);
		if (save.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
		    String file = save.getSelectedFile().getName();
		    gui.saveCalendar(file);
		} else if (JFileChooser.CANCEL_OPTION == 0) {
		    return;
		}

	    }
	});

	save.setFont(new Font("Arial", Font.PLAIN, 20));
	save.setOpaque(false);
	save.setContentAreaFilled(false);
	save.setBorderPainted(false);
	return save;
    }

    protected JButton addStageOrArtistButton(final String buttonName) {
	JButton addStageOrArtist = new JButton(buttonName);
	addStageOrArtist.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		String name;
		if (buttonName.equals("Add Stage")) {
		    name = "stage";
		} else {
		    name = "artist";
		}
		gui.addStageOrArtist(name);
	    }
	});

	addStageOrArtist.setFont(new Font("Arial", Font.PLAIN, 20));
	addStageOrArtist.setOpaque(false);
	addStageOrArtist.setContentAreaFilled(false);
	addStageOrArtist.setBorderPainted(false);
	return addStageOrArtist;
    }

    protected JButton newFile() {
	JButton newFile = new JButton("New");
	newFile.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		gui.clearGui();
	    }
	});

	newFile.setFont(new Font("Arial", Font.PLAIN, 20));
	newFile.setOpaque(false);
	newFile.setContentAreaFilled(false);
	newFile.setBorderPainted(false);
	return newFile;
    }

    protected JButton remove() {
	JButton remove = new JButton("Remove");
	remove.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		gui.removeAct();
	    }
	});

	remove.setFont(new Font("Arial", Font.PLAIN, 20));
	remove.setOpaque(false);
	remove.setContentAreaFilled(false);
	remove.setBorderPainted(false);
	return remove;
    }

    protected JButton edit() {
	JButton edit = new JButton("Edit");
	edit.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent event) {
		gui.edit();
	    }
	});

	edit.setFont(new Font("Arial", Font.PLAIN, 20));
	edit.setOpaque(false);
	edit.setContentAreaFilled(false);
	edit.setBorderPainted(false);
	return edit;
    }
}
