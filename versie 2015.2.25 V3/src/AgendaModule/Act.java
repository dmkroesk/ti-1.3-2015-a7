
package AgendaModule;
import java.util.Date;

public class Act {

    private int StageNumber;
    private Time time;
    private Artist artist;
    private Stage stage;
    private int fame;

    public Act(Stage stage, Time time, Artist artist, int fame) {
	this.stage = stage;
	this.time = time;
	this.artist = artist;
	this.fame = fame;
    }

    public void setStart(Date start) {
	time.setStart(start);
    }

    public void setEnd(Date end) {
	time.setEnd(end);
    }

    public void setArtistName(String name) {
	artist.setName(name);
    }

    public String getStart() {
	return time.getStart();
    }

    public String getEnd() {
	return time.getEnd();
    }

    public String getLength() {
	return time.getLength();
    }

    public String getArtistName() {
	return artist.getName();
    }

    public int getStageNumber() {
	return StageNumber;
    }

    public String getStageName() {
	return stage.getName();
    }

    public void setFame(int fame) {
	this.fame = fame;
    }

    public int getFame() {
	return fame;
    }

    public String toString() {
	return time.toString() + artist.toString() + "//" + stage.toString() + "//" + fame + "//";
    }

    public void setStageName(String name) {
	stage.setName(name);
    }
    
    public Stage getStage(){
	return stage;
    }
}
