package Simulator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import AgendaModule.GUI;
import Items.Bar;
import Items.FirstAid;
import Items.Food;
import Items.Pin;
import Items.Stage;
import Items.Toilet;
import Items.Waypoint;
import bezoeker.Task;

public class GuiSimulator extends JFrame {
	private static final long serialVersionUID = 1L;
	private SimulatorPanel simPanel;
	private ObjectPanel obPanel;
	private int posX, posY;

	private final int WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize()
			.getWidth();
	private final int HEIGHT = (int) Toolkit.getDefaultToolkit()
			.getScreenSize().getHeight();
	protected GUI gui;
	private MenuButtons menuButtons;

	@SuppressWarnings("unused")
	private JScrollPane listScrollPane;

	public GuiSimulator(GUI gui) {
		menuButtons = new MenuButtons(gui, this);
		this.gui = gui;

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
		addDragListeners();
		buildFrame();
		pack();
		setSize(WIDTH, HEIGHT);
		simPanel.changeTaskPlace((this.getWidth()/2) - obPanel.getWidth()/2);
		simPanel.setSize(this.getWidth()-obPanel.getWidth(), this.getHeight()-menuButtons.getHeight());
	}

	public void buildFrame() {
		obPanel = new ObjectPanel(this, gui);
		simPanel = new SimulatorPanel(obPanel, this, menuButtons);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		simPanel.addObjectPanel(obPanel);

		JPanel background = new JPanel(new BorderLayout());

		JPanel backgroundNorth = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel backgroundSouth = new JPanel(new FlowLayout(FlowLayout.LEFT));
		backgroundNorth.setBackground(Color.white);

		menuButtons.addSim(simPanel);

		backgroundNorth.add(menuButtons.NewFile());
		backgroundNorth.add(menuButtons.Load());
		backgroundNorth.add(menuButtons.Save());
		backgroundNorth.add(menuButtons.Help());
		backgroundNorth.add(menuButtons.Options());
		backgroundNorth.add(menuButtons.Quit());
		
		backgroundSouth.add(menuButtons.Start());
		backgroundSouth.add(menuButtons.Reset());

		background.add(obPanel, BorderLayout.EAST);
		background.add(backgroundNorth, BorderLayout.NORTH);
		background.add(backgroundSouth, BorderLayout.SOUTH);
		background.add(simPanel, BorderLayout.CENTER);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) (screenSize.getWidth() / 2) - (WIDTH / 2);
		int height = (int) (screenSize.getHeight() / 2) - (HEIGHT / 2);
		setUndecorated(true);
		setLocation(width, height);
		setContentPane(background);
		requestFocus(true);
		setVisible(true);
	}

	public void addDragListeners() {
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				posX = e.getX();
				posY = e.getY();
			}
		});
		this.addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent evt) {
				setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen()
						- posY);
			}
		});
	}

	/*
	 * make in this method the object of new supply's
	 */
	public void addSimSupplyObject(String name) {
		if (name.equals("Toilet"))
			simPanel.addToilet(new Toilet(new Point2D.Double(200, 200)));
		if (name.equals("Food"))
			simPanel.addFood(new Food(new Point2D.Float(200, 200)));
		if (name.equals("First aid"))
			simPanel.addFisrtAid(new FirstAid(new Point2D.Float(200, 200)));
		if (name.equals("Waypoint"))
			simPanel.addWayPoint(new Waypoint(new Point2D.Float(200, 200),
					simPanel));
		if (name.equals("Pin"))
			simPanel.addPin(new Pin(new Point2D.Float(200, 200)));
		if (name.equals("Bar"))
			simPanel.addBar(new Bar(new Point2D.Float(200, 200)));
	}

	public void addSimStageObject(String name, int x, int y) {
		simPanel.addStage(new Stage(name, new Point2D.Double(x, y), this));
	}

	public SimulatorPanel getSimulatorPanel() {
		return simPanel.getSimulatorPanel();
	}
	
	public GUI getGui()
	{
		return gui;
	}
	
	public ObjectPanel getObjectPanel()
	{
		return obPanel;
	}
	
	
}
