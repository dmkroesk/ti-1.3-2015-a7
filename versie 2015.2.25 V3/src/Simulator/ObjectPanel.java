package Simulator;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import AgendaModule.Act;
import AgendaModule.GUI;
import AgendaModule.Stage;
import Items.Toilet;

/*
 * This class makes the east of the borderLayout of the simulator. 
 */
@SuppressWarnings("serial")
public class ObjectPanel extends JPanel
{
	
	private GuiSimulator simulator;
	protected ArrayList<JButton> objectButtons = new ArrayList<JButton>();
//	private ArrayList<JButton> buttons;
	protected ArrayList<Stage> stages;
	
	/*
	 * constructor, 
	 * @param goi: this is an object of the calendar class. All stages are taken therefrom 
	 */
	public ObjectPanel(GuiSimulator simulator, GUI gui)
	{
		this.simulator = simulator;
		panel(gui);
	}
	
	/*
	 * this method makes the east panel. And add for each stage and supply a button in this panel.
	 */
	public void panel(GUI gui)
	{
		ArrayList<Stage> stages = getStages(gui);								//getStages(gui) returns an array with all the stages from the calendar
		objectButtons = new ArrayList<JButton>();
		
		for(Stage s : stages)
		{
			objectButtons.add(objectStageButton(s.getName(),"src/images/Stage.png"));
		}
		objectButtons.add(objectSupplyButton("Toilet","src/images/Toilet.png"));
		objectButtons.add(objectSupplyButton("Food","src/images/Food.png"));
		objectButtons.add(objectSupplyButton("First aid","src/images/Firstaid.png"));
		objectButtons.add(objectSupplyButton("Pin", "src/images/pin.png"));
		objectButtons.add(objectSupplyButton("Bar", "src/images/Bar.png"));
		objectButtons.add(objectSupplyButton("Waypoint", "src/images/waypoint.png"));
		
		this.setLayout(new GridLayout(objectButtons.size(), 1, 10, 10));
		for(JButton b: objectButtons)
			this.add(b);
	}

	/*
	 * this method makes a JButton when the user clicks on the button the simulator will draw the stage on the simulator panel and the button will disappear
	 * @param stageName: this is the name of the stage. Used for the name of the button
	 * @param stageSource: this is the path of the stage image. 
	 */
	public JButton objectStageButton(String stageName, String stageSource) {
		ImageIcon stagePic = new ImageIcon(stageSource);
		
		JButton button = new JButton("Stage: " + stageName, stagePic); 
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
            	simulator.addSimStageObject(stageName, 100, 100); 	//this method passes the stageName and point to GuiSimulator
            	removeButton(stageName);							//this method remove the button when the user click on it.
            }
        });
        button.setName(stageName);
        button.setBackground(Color.white);
        button.setFont(new Font("Arial", Font.PLAIN, 20));
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        return button;
	}
	
	/*
	 * this method makes a JButton when the user clicks on the button the simulator will draw the supply on the simulator panel and the button will stay
	 * @param supplySource: this is the path of the supply image. 
	 */
	public JButton objectSupplyButton(String name,String supplySource) {
		ImageIcon stagePic = new ImageIcon(supplySource);
		
		JButton button = new JButton(name, stagePic);

        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent event)
            {
            	
            	simulator.addSimSupplyObject(name);			//this method passes the stageName and point to GuiSimulator
            	
            }      
        });
        button.setBackground(Color.white);
        button.setFont(new Font("Arial", Font.PLAIN, 20));
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        return button;
	}
	
	/*
	 * this method return an ArrayList with all the stages which stay in the GUI gui object 
	 * @param gui: this is an object of the whole calendar
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Stage> getStages(GUI gui)
	{		
		ArrayList<Stage> stages = new ArrayList<Stage>();
		
		ArrayList<Act> acts = gui.passArray();
		
		for(Act a : acts)
		{
			boolean exist = false;
			for(Stage s : stages)
			{
				if(a.getStageName().equals(s.getName()))
					exist = true;
			}
			if(!exist){
				stages.add(a.getStage());
			}
		}
		this.stages = stages;
		return stages;
	}
	
	public ArrayList<Stage> getStages(){
		return stages;
	}
	

	public void addStage(String name) {
		JButton button = objectStageButton(name,"src/images/Stage.png");
		objectButtons.add(button);
		this.add(button);
		validate();
		repaint();
	}

	
	/*
	 * this method removes the button with the same name as @param name
	 */
	public void removeButton(String name) {
		
		for(Iterator<JButton> iterator = objectButtons.iterator(); iterator.hasNext();)
		{
			JButton button = iterator.next();
			if (button.getName() != null && button.getName().equals(name))
			{
				objectButtons.remove(button);
				this.remove(button);
				validate();
				repaint();
				return;
			}
		}
	}
}