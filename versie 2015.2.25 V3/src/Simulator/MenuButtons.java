package Simulator;

import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;

import bezoeker.Bezoeker;
import bezoeker.Task;
import AgendaModule.GUI;
import AgendaModule.Stage;
import Items.Food;

public class MenuButtons extends JPanel {
	private static final long serialVersionUID = 1L;
	GUI gui;
	GuiSimulator simulator;
	SimulatorPanel simulatorpanel;
	ArrayList<Bezoeker> bezoekers = new ArrayList<>();
	Task task;
	private Boolean startBoolean = false;
	int aantalBezoekers = 100;
	int intHeight = (int) Toolkit.getDefaultToolkit().getScreenSize()
			.getHeight();
	int intWidth = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	int intSpeed = 30;

	public MenuButtons(GUI gui, GuiSimulator simulator) {
		this.gui = gui;
		this.simulator = simulator;
	}

	public JButton NewFile() {
		JButton newFile = new JButton("New");
		newFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				simulator.dispose();
				simulator = new GuiSimulator(gui);

			}
		});

		newFile.setFont(new Font("Arial", Font.PLAIN, 20));
		newFile.setOpaque(false);
		newFile.setContentAreaFilled(false);
		newFile.setBorderPainted(false);
		return newFile;
	}

	public JButton Load() {
		JButton load = new JButton("Load");
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				loadSimulator();
			}
		});

		load.setFont(new Font("Arial", Font.PLAIN, 20));
		load.setOpaque(false);
		load.setContentAreaFilled(false);
		load.setBorderPainted(false);
		return load;
	}

	public JButton Save() {
		JButton load = new JButton("Save");
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				saveSimulator();
			}
		});

		load.setFont(new Font("Arial", Font.PLAIN, 20));
		load.setOpaque(false);
		load.setContentAreaFilled(false);
		load.setBorderPainted(false);
		return load;
	}

	public JButton Help() {
		JButton help = new JButton("Help");
		help.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "Made by a7");
			}
		});

		help.setFont(new Font("Arial", Font.PLAIN, 20));
		help.setOpaque(false);
		help.setContentAreaFilled(false);
		help.setBorderPainted(false);
		return help;
	}

	public JButton Options() {
		JButton Options = new JButton("Options");
		Options.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {

				// JComboBox bezoeker = new JComboBox();
				JTextField bezoeker = new JTextField();
				JTextField width = new JTextField();
				JTextField height = new JTextField();
				JTextField timeSpeed = new JTextField();
				bezoeker.setText(String.valueOf(aantalBezoekers));
				timeSpeed.setText(String.valueOf(intSpeed));
				width.setText(String.valueOf(intWidth));
				height.setText(String.valueOf(intHeight));

				Object[] optionItems = { "Amount of visitors (1 t/m 1000):",
						bezoeker, "Map width (min 500)", width,
						"Map height (min 500)", height,
						"Time speed (1 t/m 60)", timeSpeed };

				bezoeker.addKeyListener(new KeyAdapter() {
					public void keyTyped(KeyEvent e) {
						char c = e.getKeyChar();
						if (!((c >= '0' && c <= '9')
								|| c == KeyEvent.VK_BACK_SPACE
								|| c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER)) {
							JOptionPane
									.showMessageDialog(null,
											"Sorry, but that was not a valid key or format. \nValid keys: 0-9");
							e.consume();
						}
					}
				});

				width.addKeyListener(new KeyAdapter() {
					public void keyTyped(KeyEvent e) {
						char c = e.getKeyChar();
						if (!((c >= '0' && c <= '9')
								|| c == KeyEvent.VK_BACK_SPACE
								|| c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER)) {
							JOptionPane
									.showMessageDialog(null,
											"Sorry, but that was not a valid key or format. \nValid keys: 0-9");
							e.consume();
						}
					}
				});

				height.addKeyListener(new KeyAdapter() {
					public void keyTyped(KeyEvent e) {
						char c = e.getKeyChar();
						if (!((c >= '0' && c <= '9')
								|| c == KeyEvent.VK_BACK_SPACE
								|| c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER)) {
							JOptionPane
									.showMessageDialog(null,
											"Sorry, but that was not a valid key or format. \nValid keys: 0-9");
							e.consume();
						}
					}
				});

				timeSpeed.addKeyListener(new KeyAdapter() {
					public void keyTyped(KeyEvent e) {
						char c = e.getKeyChar();
						if (!((c >= '0' && c <= '9')
								|| c == KeyEvent.VK_BACK_SPACE
								|| c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER)) {
							JOptionPane
									.showMessageDialog(null,
											"Sorry, but that was not a valid key or format. \nValid keys: 0-9");
							e.consume();
						}
					}
				});

				int option = JOptionPane.showConfirmDialog(null, optionItems,
						"option", JOptionPane.OK_CANCEL_OPTION);
				if (option == JOptionPane.OK_OPTION) {
					aantalBezoekers = Integer.parseInt(bezoeker.getText());
					intHeight = Integer.parseInt(height.getText());
					intWidth = Integer.parseInt(width.getText());
					intSpeed = Integer.parseInt(timeSpeed.getText());

					if (intHeight >= 500 && intWidth >= 500) {
						simulator.getSimulatorPanel().setBackgroundSize(
								intWidth, intHeight);
					}
					if (aantalBezoekers <= 1000) {
						simulatorpanel.bezoekers(aantalBezoekers);
					}
					if (intSpeed < 70) {
						simulatorpanel.setTimeSpeed(intSpeed);
					} else {
						JOptionPane.showMessageDialog(null,
								"screen size must be at least 500 X 500");
					}
				} else if (option == JOptionPane.CANCEL_OPTION) {
					return;

					// simulator.bezoekers(aantalBezoekers);
				}
			}
		});

		Options.setFont(new Font("Arial", Font.PLAIN, 20));
		Options.setOpaque(false);
		Options.setContentAreaFilled(false);
		Options.setBorderPainted(false);
		return Options;
	}

	public JButton Quit() {
		JButton quit = new JButton("Quit");
		quit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				simulator.dispose();
			}
		});

		quit.setFont(new Font("Arial", Font.PLAIN, 20));
		quit.setOpaque(false);
		quit.setContentAreaFilled(false);
		quit.setBorderPainted(false);
		return quit;
	}

	public void loadSimulator() {
		File file;
		JFileChooser load = new JFileChooser(System.getProperty("user.dir"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"simulator file", "sima7");
		load.setFileFilter(filter);

		if (load.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			file = load.getSelectedFile();
			ArrayList<DrawObject> objects = new ArrayList<DrawObject>();
			ArrayList<Items.Stage> stages = new ArrayList<Items.Stage>();
			ArrayList<Items.Waypoint> waypoints = new ArrayList<Items.Waypoint>();
			Scanner scanner;
			try {
				scanner = new Scanner(file);
				int width = Integer.parseInt(scanner.nextLine());
				int height = Integer.parseInt(scanner.nextLine());
				int numberOfVisitors = Integer.parseInt(scanner.nextLine());

				while (scanner.hasNextLine()) {

					String[] line = scanner.nextLine().split("//");
					if (line.length >= 5) {
						String type = line[0];
						String path = line[1];
						int x = Integer.parseInt(line[2]);
						int y = Integer.parseInt(line[3]);
						double scale = Double.parseDouble(line[4]);
						double rotation = Double.parseDouble(line[5]);
						String stageName;

						DrawObject currentObject;
						switch (type) {
						case "stage":
							stageName = line[6];
							Items.Stage stage = new Items.Stage(stageName,
									new Point2D.Float(x, y), simulator);
							stage.setScale(scale);
							stage.setRotation(rotation);
							stages.add(stage);
							currentObject = null;

							break;
						case "waypoint":
							Items.Waypoint waypoint = new Items.Waypoint(
									new Point2D.Float(x, y), simulatorpanel);
							waypoint.setScale(scale);
							waypoint.setRotation(rotation);
							String[] directions = path.split(",");
							ArrayList<Integer> intDirections = new ArrayList<Integer>();
							boolean isEntrance;
							if (directions[0].equals("false"))
								isEntrance = false;
							else
								isEntrance = true;
							for (int i = 1; i < directions.length; i++) {
								intDirections.add(Integer
										.parseInt(directions[i]));
							}
							waypoint.setIsEntrance(isEntrance);
							waypoint.load(intDirections);
							waypoints.add(waypoint);
							currentObject = null;
							break;

						case "food":
							currentObject = new Items.Food(new Point2D.Float(x,
									y));
							break;

						case "bar":
							currentObject = new Items.Bar(new Point2D.Float(x,
									y));
							break;

						case "entrance":
							currentObject = new Items.Entrance(
									new Point2D.Float(x, y));
							break;
						case "firstaid":
							currentObject = new Items.FirstAid(
									new Point2D.Float(x, y));
							break;
						case "pin":
							currentObject = new Items.Pin(new Point2D.Float(x,
									y));
							break;
						case "toilet":
							currentObject = new Items.Toilet(new Point2D.Float(
									x, y));
							break;
						default:
							currentObject = new DrawObject(path,
									new Point2D.Float(x, y));
							break;
						}
						if (currentObject != null) {
							currentObject.setRotation(rotation);
							currentObject.setScale(scale);
							objects.add(currentObject);
						}
					}
				}

				scanner.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			SimulatorPanel panel = simulator.getSimulatorPanel();
			panel.resetPanel(objects);
			handleStageSimulator(stages);
			handleWayPointSimulator(waypoints);
		}
	}

	public void handleStageSimulator(ArrayList<Items.Stage> stages) {

		for (Items.Stage stage : stages) {
			ObjectPanel obPanel = simulator.getObjectPanel();
			obPanel.removeButton(stage.getStage());
			simulatorpanel.addStage(stage);
		}
	}

	public void handleWayPointSimulator(ArrayList<Items.Waypoint> waypoints) {

		for (Items.Waypoint waypoint : waypoints) {
			simulatorpanel.addWayPoint(waypoint);
		}
		simulatorpanel.repaint();
	}

	public void saveSimulator() {
		File file = null;
		JFileChooser save = new JFileChooser(System.getProperty("user.dir"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"simulator file", "sima7");
		save.setFileFilter(filter);
		if (save.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			file = save.getSelectedFile();

		}

		String filePath = file.getAbsolutePath();

		if (!filePath.endsWith(".sima7"))
			filePath = filePath + ".sima7";
		ArrayList<DrawObject> objects = simulator.getSimulatorPanel()
				.getObjects();
		ArrayList<Items.Waypoint> waypoints = simulator.getSimulatorPanel()
				.getWaypoints();
		try {

			FileWriter writerSim = new FileWriter(filePath);
			int screenWidth = simulatorpanel.getWidth();
			int screenHeight = simulatorpanel.getHeight();
			int numberOfVisitors = simulatorpanel.getBezoekers().size();
			writerSim.write(screenWidth + "\n" + screenHeight + "\n"
					+ numberOfVisitors + "\n");

			for (DrawObject o : objects) {
				String type = o.getClassName();
				String path = o.getImagePath();
				int x = o.getX();
				int y = o.getY();
				double scale = o.getScale();
				double rotation = o.getRotation();
				String stageName = o.getStage();

				writerSim.write(type + "//" + path + "//" + x + "//" + y + "//"
						+ scale + "//" + rotation + "//" + stageName + "\n");
			}

			for (Items.Waypoint w : waypoints) {
				String type = w.getClassName();
				String directions = "";
				if (w.getIsEntrance())
					directions += "true,";
				else
					directions += "false,";

				ArrayList<Integer> directionsarray = w.save();

				for (Integer d : directionsarray) {
					directions += d + ",";
				}

				int x = w.getX();
				int y = w.getY();
				double scale = w.getScale();
				double rotation = w.getRotation();
				String stageName = w.getStage();

				writerSim.write(type + "//" + directions + "//" + x + "//" + y
						+ "//" + scale + "//" + rotation + "//" + stageName
						+ "\n");
			}

			writerSim.close();

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,
					"something went wrong, try again");
		}

	}

	public void addSim(SimulatorPanel simPanel) {
		this.simulatorpanel = simPanel;
	}

	public JButton Start() {
		JButton start = new JButton("Start");
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (simulatorpanel.doButtonsOverlap()) {
					JOptionPane.showMessageDialog(null,
							"Objects may overlap please check if they do.");
				} else {
					if (simulatorpanel.ourClock.isRunning()) {
						simulatorpanel.ourClock.stop();
						start.setText("Start");
						startBoolean = false;
					} else {
						simulatorpanel.ourClock.start();
						start.setText("Pause");
						startBoolean = true;
					}
				}
			}
		});

		start.setFont(new Font("Arial", Font.PLAIN, 20));
		start.setOpaque(false);
		start.setContentAreaFilled(false);
		start.setBorderPainted(false);
		return start;
	}

	public JButton Reset() {
		JButton reset = new JButton("Reset");
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				simulatorpanel.resetTimer();
				startBoolean = false;
			}

		});

		reset.setFont(new Font("Arial", Font.PLAIN, 20));
		reset.setOpaque(false);
		reset.setContentAreaFilled(false);
		reset.setBorderPainted(false);
		return reset;
	}

	public Boolean getStartBoolean() {
		return startBoolean;
	}
}