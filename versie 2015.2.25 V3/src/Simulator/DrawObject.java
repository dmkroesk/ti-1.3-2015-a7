package Simulator;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

import bezoeker.Bezoeker;

public class DrawObject {
	Point2D position;
	double rotation;
	protected double scale;
	Image image;
	String imagePath;

	public DrawObject(String filename, int x, int y) {
		image = new ImageIcon(filename).getImage();
		scale = 1;
		rotation = 0;
		position = new Point2D.Double(x, y);
		imagePath = filename;
	}

	public DrawObject(String filename, Point2D position) {
		image = new ImageIcon(filename).getImage();
		scale = 1;
		rotation = 0;
		this.position = position;
	}

	@SuppressWarnings("unused")
	public void draw(Graphics g) {
		AffineTransform tx = getTransform();
		g.drawImage(image, (int) position.getX(), (int) position.getY(), null);
	}

	private AffineTransform getTransform() {
		AffineTransform tx = new AffineTransform();
		tx.translate(position.getX(), position.getY());
		tx.scale(scale, scale);
		tx.rotate(Math.toRadians(rotation), image.getWidth(null) / 2,
				image.getHeight(null) / 2);
		return tx;
	}

	public boolean contains(Point2D point) {

		Shape shape = new Rectangle2D.Double(0, 0, image.getWidth(null),
				image.getHeight(null));
		return getTransform().createTransformedShape(shape).contains(point);
	}

	public void setPosition(int x, int y) {
		position.setLocation(x, y);
	}

	public int getX() {
		return (int) position.getX();
	}

	public int getY() {
		return (int) position.getY();
	}

	public Point2D getPosition() {
		return position;
	}

	public void draw(Graphics2D g) {
		AffineTransform tx = getTransform();
		g.drawImage(image, tx, null);
	}

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	public String getImagePath() {
		return imagePath;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public void showPopup() {
		//override this method 
	}
	
	public void setImage(String filename){
		this.image = new ImageIcon(filename).getImage();
	}
	
	public boolean intersects(DrawObject o)
	{
		if (contains(new Point2D.Double(o.getX(), o.getY())))
			return true;
		if (contains(new Point2D.Double(o.getX(), o.getY() + o.image.getHeight(null))))
			return true;
		if (contains(new Point2D.Double(o.getX() + o.image.getWidth(null), o.getY() + o.image.getHeight(null))))
			return true;
		if (contains(new Point2D.Double(o.getX() + o.image.getWidth(null), o.getY())))
			return true;
		
		return false;
	}
	
	/*
	 * override this method in a subclass
	 */
	public String getClassName()
    {
		//override this method
    	return " ";
    }
	
	
	/*
	 * override this method
	 */
	public String getStage()
	{
		return "";
	}
}
