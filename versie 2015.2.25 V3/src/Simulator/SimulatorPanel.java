package Simulator;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.swing.Timer;
import javax.imageio.ImageIO;
import javax.swing.*;

import bezoeker.Bezoeker;
import bezoeker.Task;
import Items.*;

public class SimulatorPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private BufferedImage background;
	private ArrayList<DrawObject> objects = new ArrayList<>();
	private ArrayList<Waypoint> waypoints = new ArrayList<Waypoint>();

	DrawObject dragObject;
	Point2D lastClickPosition;
	Point lastMousePosition;
	private int vorigeX, vorigeY;
	float cameraScale = 1;
	Point2D cameraPoint = new Point2D.Double(1920 / 2, 1080 / 2);

	public static Timer ourClock;
	private Task task;
	private boolean rightMousePressed = false;
	private GuiSimulator guiSimulator;
	private double Width = Toolkit.getDefaultToolkit().getScreenSize()
			.getWidth();
	private double Height = Toolkit.getDefaultToolkit().getScreenSize()
			.getHeight();
	private ArrayList<Bezoeker> bezoekers = new ArrayList<>();
	private ObjectPanel obPanel;
	private MenuButtons buttons;

	public SimulatorPanel(ObjectPanel objectPanel, GuiSimulator guiSimulator,
			MenuButtons buttons) {
		this.guiSimulator = guiSimulator;
		this.task = new Task(this);
		this.buttons = buttons;
		ourClock = new Timer(1000 / 10, task);
		panel();
		scroll();
		dragBackground();
		bezoekers(0);
		task.setWidth(this.getWidth());
		
		Timer timer = new Timer(1000/60, this);
		timer.start();
		
		repaint();
	}

	public void bezoekers(int aantalBezoekers) {
		bezoekers.clear();
		for (int i = 0; i < waypoints.size(); i++) {
			if (waypoints.get(i).getIsEntrance()) {
				while (bezoekers.size() < aantalBezoekers) {
					Bezoeker b = new Bezoeker(waypoints.get(i).getPosition(),
							guiSimulator.getGui().getActs());
					bezoekers.add(b);
				}
			}

		}
		repaint();
	}

	public void panel() {
		try {
			background = ImageIO.read(new File(
					"src/images/Background, Grass.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;
		Graphics2D g3 = (Graphics2D) g;

		AffineTransform oldTransform = g2.getTransform();
		g2.setTransform(getCamera());

		TexturePaint p = new TexturePaint(background, new Rectangle2D.Double(0,
				0, Width, Height));
		g2.setPaint(p);
		g2.fill(new Rectangle2D.Double(0, 0, this.Width, this.Height));

		for (DrawObject a : objects)
			a.draw(g2);

		for (Waypoint a : waypoints)
			a.draw(g2);

		for (Bezoeker b : bezoekers) {
			b.paint(g2);
		}

		g2.setTransform(oldTransform);

		g3.setColor(Color.RED);
		g3.setFont(new Font("Purisa", Font.PLAIN, 30));
		task.paint(g3);
	}

	private AffineTransform getCamera() {
		AffineTransform tx = new AffineTransform();
		tx.translate(-cameraPoint.getX() + getWidth() / 2, -cameraPoint.getY()
				+ getHeight() / 2);
		tx.scale(cameraScale, cameraScale);

		return tx;
	}

	public void dragBackground() {
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {

				Point2D clickPoint = getClickPoint2doint(e.getPoint());
				lastClickPosition = clickPoint;
				vorigeX = e.getX();
				vorigeY = e.getY();

				for (DrawObject o : objects) {
					if (o.contains(clickPoint))
						dragObject = o;
				}
				for (Waypoint b : waypoints) {
					if (b.contains(clickPoint))
						dragObject = b;
				}

				if (e.isControlDown() && SwingUtilities.isRightMouseButton(e)) {
					if (!buttons.getStartBoolean()) {
						Iterator<DrawObject> it = objects.iterator();
						Iterator<Waypoint> it2 = waypoints.iterator();
						while (it.hasNext()) {
							DrawObject o = it.next();
							if (o.contains(clickPoint)) {
								if (o instanceof Stage)
									obPanel.addStage(((Stage) o).getStage());
								it.remove();
							}
						}
						while (it2.hasNext()) {
							Waypoint o = it2.next();
							if (o.contains(clickPoint)) {
								it2.remove();
							}
						}
					}
					repaint();
				} else if (SwingUtilities.isRightMouseButton(e))
					rightMousePressed = true;

				else if (e.isShiftDown()) {
					if (!buttons.getStartBoolean()) {
						for (Waypoint way : waypoints) {
							if (way.contains(clickPoint)) {
								way.showPopup();
								repaint();
							}
						}
					}

					for (DrawObject object : objects) {
						if (object.contains(clickPoint))
							object.showPopup();

					}
					dragObject = null;
				}

			}

			public void mouseReleased(MouseEvent e) {
				dragObject = null;
				rightMousePressed = false;
			}
		});

		this.addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent evt) {
				// Point2D clickPoint = getClickPoint(evt.getPoint());

				Point2D clickPoint = getClickPoint2doint(evt.getPoint());

				if (dragObject != null) {
					if (!buttons.getStartBoolean()) {
						if (SwingUtilities.isLeftMouseButton(evt)) {
							dragObject.position = new Point2D.Double(
									dragObject.position.getX()
											- (lastClickPosition.getX() - clickPoint
													.getX()),
									dragObject.position.getY()
											- (lastClickPosition.getY() - clickPoint
													.getY()));
							// dragObject.position.getX()
							// - (vorigeX - evt.getX()),
							// dragObject.position.getY()
							// - (vorigeY - evt.getY()));

							vorigeX = evt.getX();
							vorigeY = evt.getY();
						}
					}
				}

				else {
					if (SwingUtilities.isLeftMouseButton(evt)) {
						cameraPoint = new Point2D.Double(cameraPoint.getX()
								+ (vorigeX - evt.getX()), cameraPoint.getY()
								+ (vorigeY - evt.getY()));

						vorigeX = evt.getX();
						vorigeY = evt.getY();

					}
				}
				lastClickPosition = clickPoint;
				repaint();
			}

		});
		repaint();
	}

	private Point2D getClickPoint2doint(Point point) {
		try {
			return getCamera().inverseTransform(point, null);
		} catch (NoninvertibleTransformException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	public void scroll() {
		addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				Point2D clickPoint = getClickPoint2doint(e.getPoint());
				if (!buttons.getStartBoolean()) {
					for (DrawObject o : objects) {
						if (o.contains(clickPoint)) {
							if (!rightMousePressed)
								o.scale *= 1 - (e.getPreciseWheelRotation() / 10.0);
							else
								dragObject.rotation += e
										.getPreciseWheelRotation() * 30;
							repaint();
							return;
						}

					}

					for (Waypoint o : waypoints) {
						if (o.contains(clickPoint)) {
							if (!rightMousePressed)
								o.scale *= 1 - (e.getPreciseWheelRotation() / 10.0);
							else
								dragObject.rotation += e
										.getPreciseWheelRotation() * 30;
							repaint();
							return;
						}

					}
				}

				// cameraPoint
				cameraScale *= 1 - (e.getPreciseWheelRotation() / 10.0);
				repaint();
			}
		});

	}

	public void addStage(Stage stage) {
		objects.add(stage);
		repaint();
	}

	public void addToilet(Toilet toilet) {
		objects.add(toilet);
		repaint();
	}

	public void addFood(Food food) {
		objects.add(food);
		repaint();
	}

	public void addFisrtAid(FirstAid firstaid) {
		objects.add(firstaid);
		repaint();
	}

	public void addWayPoint(Waypoint waypoint) {
		waypoints.add(waypoint);
		Waypoint a = waypoints.get(waypoints.size() - 1);
		a.setNumber(waypoints.size() - 1);
		repaint();
	}

	public void addPin(Pin pin) {
		objects.add(pin);
		repaint();
	}

	public void addBar(Bar bar) {
		objects.add(bar);
		repaint();
	}

	public SimulatorPanel getSimulatorPanel() {
		return this;
	}

	public ArrayList<DrawObject> getObjects() {
		return objects;
	}

	public void resetPanel(ArrayList<DrawObject> objects) {
		this.objects = objects;
		waypoints.clear();
		repaint();
	}

	public double width() {
		return Width;
	}

	public double heigth() {
		return Height;
	}

	public void addObjectPanel(ObjectPanel obPanel) {
		this.obPanel = obPanel;

	}

	public ArrayList<AgendaModule.Stage> getStages() {
		return obPanel.getStages();
	}

	public ArrayList<Waypoint> getWaypoints() {
		return waypoints;
	}

	public void changeTaskPlace(int place) {
		task.changePlace(place);
	}

	public void setBackgroundSize(int width, int height) {
		this.Height = height;
		this.Width = width;
		repaint();
	}

	public ArrayList<Bezoeker> getBezoekers() {
		return bezoekers;
	}

	public boolean doButtonsOverlap() {
		for (DrawObject ob1 : objects) {
			for (DrawObject ob2 : objects) {
				if (ob1 != ob2) {
					if (ob1.intersects(ob2))
						return true;
				}
			}
		}
		return false;
	}

	public void setTimeSpeed(int intSpeed) {
		ourClock = new Timer(1000/intSpeed, task);
		
	}

	public void resetTimer() {
		this.ourClock.restart();
		this.ourClock.stop();
		task.restartTime();
		repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
			repaint();
			for(Bezoeker b : bezoekers)
				b.update(30.0);
	}

}
