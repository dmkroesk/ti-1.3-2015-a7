package bezoeker;

public class Route 
{
	public int startTime;
	public int endTime;
	public String stage;
	
	public Route(int start, int end, String stage)
	{
		startTime = start;
		endTime = end;
		this.stage = stage;
	}
	
	public int returnStartTime()
	{
		return startTime;
	}
	
	public int returnEndTime()
	{
		return endTime;
	}
	
	public String returnStage()
	{
		return stage;
	}
}
