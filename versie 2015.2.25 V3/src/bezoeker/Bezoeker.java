package bezoeker;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import AgendaModule.Act;

public class Bezoeker 
{
	private ArrayList <Route> route = new ArrayList<>();
	private ArrayList<Act> acts = new ArrayList<>();
	private ConvertTime timeCoverter;
	private boolean atStage = false;
	public int index;
	double rotation;
	Point2D position;
	Point2D target;
	Image image = new ImageIcon("src/images/Personen.png").getImage();
	private boolean isAtStage;

	//1440 minuten per dag
	public Bezoeker(Point2D position, ArrayList<Act> acts)
	{
		index = 0;
		this.acts = acts;
		this.position = position;
		route();
	}

	public void route()
	{
		timeCoverter = new ConvertTime(this);
		int endTime = timeCoverter.returnEnd(acts.get(0));

		for (Act a: acts)
		{
			int x = (int) (Math.random()*6);
			
			if (endTime < (timeCoverter.returnStart(a)))
			{
				atStage = false;
			}
			
			if((a.getFame()/20) > x && !atStage)
			{
				route.add(new Route(timeCoverter.returnStart(a), timeCoverter.returnEnd(a), a.getStageName()));
				atStage = true;
			}	
			endTime = timeCoverter.returnEnd(a);
		}
	}
	
	public void update(double speed){
		update(target, speed);
	}
	
	public void update(Point2D target, double speed)
	{
		this.target = target;
		Point2D difference = new Point2D.Double(target.getX() - position.getX(), target.getY() - position.getY());
		
		double newRotation = Math.atan2(difference.getY(), difference.getX());
		double rotDifference = rotation - newRotation;
		while(rotDifference > Math.PI)
			rotDifference -= 2 * Math.PI;
		while(rotDifference < -Math.PI)
			rotDifference += 2 * Math.PI;
		
		if(Math.abs(rotDifference) < 0.1)
			rotation = newRotation;
		else if(rotDifference < 0)
			rotation += 0.1;
		else if(rotDifference > 0)
			rotation -= 0.1;
		position = new Point2D.Double(position.getX() + speed * Math.cos(rotation), position.getY() + speed * Math.sin(rotation));
	}
	
	public boolean hasCollision(ArrayList<Bezoeker> bezoekers) {
		for(Bezoeker b : bezoekers)
		{
			if(b == this)
				continue;
			if(b.position.distance(position) < 10)
				return true;
		}
		return false;
	}
	
	public Point2D getPos() 
	{
		return position;
	}
	
	public ArrayList<Route> returnRoute()
	{
		return route;
	}
	
	public void paint(Graphics2D g)
	{	
		AffineTransform tx = new AffineTransform();
		tx.translate(position.getX()-5, position.getY()-5);
		tx.rotate(rotation, 5, 5);
		g.drawImage(image, tx ,null);
	}
	
	public int changeIndex()
	{
		return index++;
	}
	
	public int returnIndex()
	{
		return index;
	}
	
	public boolean changeStatus(boolean s)
	{
		isAtStage = s;
		return isAtStage;
	}
	
	public boolean returnStatus()
	{
		return isAtStage;
	}
}

