package bezoeker;

import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.TimerTask;

import Items.Waypoint;
import Simulator.SimulatorPanel;

public class Task extends TimerTask implements ActionListener{

	private int  timeInMin= 0, timeInHours=0;
	private SimulatorPanel simulatorpanel;
	private ArrayList<Waypoint> way = new ArrayList<>();
	private int width = Toolkit.getDefaultToolkit().getScreenSize().width/2;
	Point2D target;
	private int timeInMinutes = 0;
	private int i = 0;
	private int target2;
	private String naam;
	
	public Task(SimulatorPanel SimulatorPanel) {
		this.simulatorpanel = SimulatorPanel;
		way = simulatorpanel.getWaypoints();
	}

	public void run()
	{
		timeInMinutes++;
		this.timeInMin++;
		if(timeInMin == 60)
		{
			timeInMin = 0;
			timeInHours ++;
		}
		if(timeInHours == 24)
		{
			timeInHours = 24;
			timeInMin = 0;
			simulatorpanel.ourClock.stop();
		}
		ArrayList<Bezoeker> bezoekers = simulatorpanel.getBezoekers();
		for(Bezoeker b: bezoekers)
		{
			if ( b.returnIndex() < b.returnRoute().size())
			{
//				if(b.returnRoute().get(b.returnIndex()).returnStartTime() == timeInMinutes)
				if(b.returnRoute().get(b.returnIndex()).returnStartTime()-30 <= timeInMinutes &&
						b.returnRoute().get(b.returnIndex()).returnEndTime()+15 > timeInMinutes)
				{
					naam = b.returnRoute().get(b.returnIndex()).returnStage();
					for (Waypoint w: way)
						if(w.contains(b.getPos()))
							target2 = w.getWaypointNumber(naam);
					
					
					target = way.get(target2).getPosition();
					b.update(target, (Math.random()*5));
					b.returnStatus();
				}
				else if(b.returnRoute().get(b.returnIndex()).returnEndTime() == timeInMinutes)
				{
					b.changeIndex();
					target = way.get(0).getPosition();
					b.update(target, (Math.random()*15));
				}
			}
			else
			{
				target = way.get(0).getPosition();
				b.update(target, (Math.random()*5));
			}
		}
		simulatorpanel.repaint(); 
	}
	
	public void paint(Graphics2D g)
	{
		g.drawString(Integer.toString(timeInHours)+":"+Integer.toString(timeInMin), width, 30);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		run();
		
	}
	
	public void changePlace(int width){
		this.width = width;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
		System.out.println(width);
	}
	
	public void restartTime(){
		this.timeInMin = 0;
		this.timeInHours = 0;
		this.timeInMinutes = 0;
	}
}
