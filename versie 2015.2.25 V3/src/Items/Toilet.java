package Items;

import java.awt.geom.Point2D;

import Simulator.DrawObject;

public class Toilet extends DrawObject {

	public Toilet(Point2D position) {
		super("src/images/Toilet.png", position);
		// TODO Auto-generated constructor stub
	}
	
	public String getImagePath()
	{
		return "src/images/Toilet.png";
	}
	
    public String getClassName()
    {
    	return "toilet";
    }
}
