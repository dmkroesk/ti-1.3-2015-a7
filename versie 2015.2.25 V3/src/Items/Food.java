package Items;

import java.awt.geom.Point2D;

import Simulator.DrawObject;

public class Food extends DrawObject {
	public Food(Point2D position) {
		super("src/images/Food.png", position);
	}
	
	public String getImagePath()
	{
		return "src/images/Food.png";
	}
	
    public String getClassName()
    {
    	return "food";
    }
}
