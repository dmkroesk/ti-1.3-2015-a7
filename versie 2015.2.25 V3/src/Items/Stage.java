package Items;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import AgendaModule.Act;
import AgendaModule.GUI;
import Simulator.DrawObject;
import Simulator.GuiSimulator;

public class Stage extends DrawObject {
	private String stageName;
	private GuiSimulator guiSimulator; 
	private ArrayList<Act> acts;
	
	
	public Stage(String stageName, Point2D position, GuiSimulator gui) 
	{
		super("src/images/Stage.png", position);
		this.stageName = stageName;
		guiSimulator = gui;
		acts = new ArrayList<Act>();
		getActs();
	}
	
	public void setstageName(String stageName)
	{
		this.stageName = stageName;
	}
	
	public String getStage()
	{
		return stageName;
	}
	
	public String getImagePath()
	{
		return "src/images/Stage.png";
	}
	
	public void draw(Graphics2D g){
		super.draw(g);
		g.setColor(Color.RED);
		g.setFont(new Font("Purisa", Font.PLAIN, 30));
		g.drawString(stageName, super.getX(), super.getY());
	}
	
	
	public void showPopup()
	{
		JOptionPane.showMessageDialog(null,
			    "acts: " + System.lineSeparator() + actsToString(),"stage: " + stageName,
			    JOptionPane.PLAIN_MESSAGE);
	}
	
	public void getActs()
	{
		GUI gui = guiSimulator.getGui();
		ArrayList<Act> allacts = gui.getActs();
		for(Act a: allacts)
		{
			if(a.getStageName().equals(stageName))
				acts.add(a);
		}

	}
	public String actsToString()
	{
		String stringActs = "";
		for(Act a : acts)
		{
			stringActs +=  "   artist: "+ a.getArtistName() + "\n   start time: " + a.getStart() + "\n   end time: " + a.getEnd() + "\n " + System.lineSeparator();
		}
		return stringActs;
	}
	
	public String getClassName()
	{
		return "stage";
	}
}
