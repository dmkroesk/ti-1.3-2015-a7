package Items;

import java.awt.geom.Point2D;

import Simulator.DrawObject;

public class FirstAid extends DrawObject {

	public FirstAid(Point2D position) {
		super("src/images/Firstaid.png", position);
	}
	
	public String getImagePath()
	{
		return "src/images/Firstaid.png";
	}
   
	public String getClassName()
    {
    	return "firstaid";
    }
}

