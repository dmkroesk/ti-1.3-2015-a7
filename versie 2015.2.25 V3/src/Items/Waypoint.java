package Items;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.*;

import javax.swing.*;

import Simulator.*;

public class Waypoint extends DrawObject {
	private SimulatorPanel sim;
	private ArrayList<AgendaModule.Stage> stages;
	private ArrayList<Waypoint> waypoints;
	private int number;
	private ArrayList<Integer> waypointCombos;
	private boolean isEntrance;
	private ArrayList<Object> objectsArrayList;

	public Waypoint(Point2D position, SimulatorPanel sim) {
		super("src/images/waypoint.png", position);
		this.sim = sim;
		this.isEntrance = false;
		objectsArrayList = new ArrayList<Object>();
		getWaypoints();
		getStages();

		waypointCombos = new ArrayList<>();
		for (int i = 0; i < stages.size(); i++) {
			waypointCombos.add(0);
		}
		fillArrayList();
	}

	@Override
	public String getImagePath() {
		return "src/images/waypoint.png";
	}

	public void getStages() {
		this.stages = sim.getStages();
	}

	public void getWaypoints() {
		this.waypoints = sim.getWaypoints();
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	@Override
	public void showPopup() {
		fillArrayList();

		Object[] message = objectsArrayList.toArray();

		int option = JOptionPane.showConfirmDialog(null, message,
				"Edit Waypoint " + number, JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		if (option == JOptionPane.CANCEL_OPTION
				|| option == JOptionPane.CLOSED_OPTION)
			return;
		if (option == JOptionPane.OK_OPTION) {
			for (int i = 3; i < objectsArrayList.size(); i += 2) {
				JComboBox<Integer> jComboBox = (JComboBox<Integer>) objectsArrayList
						.get(i);
				int a = Integer
						.parseInt(jComboBox.getSelectedItem().toString());
				waypointCombos.set((i - 3) / 2, a);
			}

			JCheckBox checkbox2 = (JCheckBox) objectsArrayList.get(1);
			if (checkbox2.isSelected()) {
				this.isEntrance = true;
			} else if (!checkbox2.isSelected()) {
				if (isEntrance) {
					isEntrance = false;
				} else {
					return;
				}
			}
		}
	}

	private void fillArrayList() {
		objectsArrayList.clear();
		getWaypoints();
		JLabel label1 = new JLabel("Entrance: ");
		objectsArrayList.add(label1);
		JCheckBox checkbox = new JCheckBox("Entrance", false);
		checkbox.setSelected(isEntrance);
		objectsArrayList.add(checkbox);
		for (int i = 0; i < stages.size(); i++) {
			JLabel label = new JLabel(stages.get(i).getName());
			objectsArrayList.add(label);
			objectsArrayList.add(getwaypointNumbers(i));
		}

	}

	private JComboBox<Integer> getwaypointNumbers(int index) {
		JComboBox<Integer> waypointNumbers = new JComboBox<>();
		for (Waypoint way : waypoints) {
			waypointNumbers.addItem(way.getNumber());
			waypointNumbers.setEditable(false);

			try {
				waypointNumbers.setSelectedItem(waypointCombos.get(index));
			} catch (IndexOutOfBoundsException e) {
				System.out.println("Out of bounds");
			}
		}
		return waypointNumbers;
	}

	@Override
	public void draw(Graphics2D g) {
		super.draw(g);
		if (isEntrance) {
			super.setImage("src/images/entrance.png");
		} else {
			super.setImage("src/images/waypoint.png");
		}
		g.setColor(Color.RED);
		g.setFont(new Font("Purisa", Font.PLAIN, 30));
		g.drawString("" + number, super.getX(), super.getY());
	}

	/*
	 * @param stageNumber from stageArrayList from simulatorPanel
	 * 
	 * @return waypointNumber
	 */
	public int getWaypointNumber(int index) {
		index += 2;
		return waypointCombos.get(index);
	}

	/*
	 * @param stageName
	 * 
	 * @return waypointNumber
	 */
	public int getWaypointNumber(String stageName) {
		for (int i = 0; i < stages.size(); i++) {
			if (stages.get(i).getName().equals(stageName)) {
				return waypointCombos.get(i);
			}
		}
		return 0;
	}

	/*
	 * @return boolean to check isEntrance
	 */
	public boolean getIsEntrance() {
		return isEntrance;
	}

	public void setIsEntrance(boolean isEntrance) {
		this.isEntrance = isEntrance;
		if (isEntrance) {
			super.setImage("src/images/waypoint.png");
			sim.repaint();
		} else {
			return;
		}
	}

	@Override
	public String getClassName() {
		return "waypoint";
	}

	public ArrayList<Integer> save() {
		return waypointCombos;
	}

	public void load(ArrayList<Integer> waypointCombos) {
		this.waypointCombos = waypointCombos;
	}
}
