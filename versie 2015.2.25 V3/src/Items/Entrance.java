package Items;

import java.awt.geom.Point2D;

import Simulator.DrawObject;

public class Entrance extends DrawObject {

	public Entrance(Point2D position) {
		super("src/images/entrance.png", position);
	}
	
	public String getImagePath()
	{
		return "src/images/entrance.png";
	}
	
    public String getClassName()
    {
    	return "entrance";
    	
    }
}

