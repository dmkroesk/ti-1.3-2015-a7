package Items;

import java.awt.geom.Point2D;

import Simulator.DrawObject;

public class Bar extends DrawObject {
	
	public Bar(Point2D position) {
		super("src/images/Bar.png", position);
	}
	
	public String getImagePath()
	{
		return "src/images/Bar.png";
	}
	
    public String getClassName()
    {
    	return "bar";
    }
	
}
