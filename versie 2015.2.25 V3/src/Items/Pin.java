package Items;

import java.awt.geom.Point2D;

import Simulator.DrawObject;

public class Pin extends DrawObject {
	
	public Pin(Point2D position) {
		super("src/images/Pin.png", position);
	}
	
	public String getImagePath()
	{
		return "src/images/Pin.png";
	}
	
    public String getClassName()
    {
    	return "pin";
    }
	
}
